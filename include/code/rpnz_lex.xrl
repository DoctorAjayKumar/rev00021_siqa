% File: rpnz_lex.xrl

Definitions.

INT        = -?[0-9]+
WHITESPACE = [\s\t\n\r]+

Rules.

INT -> {token, {int, TokenLine, s2i(TokenChars)}}.
\+  -> {token, {op, TokenLine, s2a(TokenChars)}}.
\*  -> {token, {op, TokenLine, s2a(TokenChars)}}.

Erlang code.

s2i(List) ->
    erlang:list_to_integer(List).

s2a("+") -> '+';
s2a("*") -> '*'.
