% this is like a stack collapser
% rpn is binary isn't it

% Terminals are the parts of speech
Terminals int op.

% Nonterminals are the nodes higher up in the tree
Nonterminals
    args
    int_leaf
    ints
    op_leaf
    sentence
    .

% Root symbol is the type of node at the top of the tree
Rootsymbol sentence.


% a sentence can be just a plain operator, or a list of ints followed
% by an operator, or arguments followed by an operator
sentence -> op_leaf.
sentence -> args op_leaf.

args -> int_leaf.
args -> int_leaf args.
args -> sentence.
args -> sentence args.

int_leaf -> int : {leaf, '$1'}.
op_leaf  -> op  : {leaf, '$1'}.
