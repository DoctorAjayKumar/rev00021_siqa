% File: simple1_parse.yrl

Nonterminals sentence noun_phrase verb_phrase.
Terminals noun verb.
Rootsymbol sentence.

sentence    -> noun_phrase verb_phrase : {stem, sentence, ['$1', '$2']}.
noun_phrase -> noun                    : {stem, noun_phrase, ['$1']}.
verb_phrase -> verb                    : {stem, verb_phrase, ['$1']}.
