% File: simple1_lex.xrl

Definitions.

WHITESPACE = [\s\t\n\r]

Rules.

i               : {token, {noun, TokenLine, TokenChars}}.
everybody       : {token, {noun, TokenLine, TokenChars}}.
he              : {token, {noun, TokenLine, TokenChars}}.
know            : {token, {verb, TokenLine, TokenChars}}.
poops           : {token, {verb, TokenLine, TokenChars}}.
is              : {token, {verb, TokenLine, TokenChars}}.
{WHITESPACE}+   : skip_token.

Erlang code.
