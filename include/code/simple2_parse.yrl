% File: simple2_parse.yrl

Nonterminals sentence noun_phrase verb_phrase.
Terminals noun verb.
Rootsymbol sentence.

sentence    -> noun_phrase verb_phrase : {stem, sentence, ['$1', '$2']}.
noun_phrase -> noun                    : {stem, noun_phrase, [{leaf, '$1'}]}.
verb_phrase -> verb                    : {stem, verb_phrase, [{leaf, '$1'}]}.
